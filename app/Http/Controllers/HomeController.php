<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // index
    public function index() {
        return view('index');
    }

    public function register() {
        return view('form');
    }

    public function welcome(Request $request) {
        $fname = $request->input('fname');
        $lname = $request->input('lname');

        return view('dashboard', ['fname' => $fname, 'lname' => $lname]);
        
    }
}
